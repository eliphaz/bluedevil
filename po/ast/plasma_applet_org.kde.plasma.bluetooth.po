# Copyright (C) 2023 This file is copyright:
# This file is distributed under the same license as the bluedevil package.
#
# SPDX-FileCopyrightText: 2023 Enol P. <enolp@softastur.org>
msgid ""
msgstr ""
"Project-Id-Version: bluedevil\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2024-06-21 00:39+0000\n"
"PO-Revision-Date: 2023-11-01 12:32+0100\n"
"Last-Translator: Enol P. <enolp@softastur.org>\n"
"Language-Team: \n"
"Language: ast\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 23.08.2\n"

#: package/contents/ui/DeviceItem.qml:41
#, kde-format
msgid "Disconnect"
msgstr ""

#: package/contents/ui/DeviceItem.qml:41
#, kde-format
msgid "Connect"
msgstr ""

#: package/contents/ui/DeviceItem.qml:50
#, kde-format
msgid "Browse Files"
msgstr ""

#: package/contents/ui/DeviceItem.qml:61
#, kde-format
msgid "Send File"
msgstr ""

#: package/contents/ui/DeviceItem.qml:118
#, kde-format
msgid "Copy"
msgstr ""

#: package/contents/ui/DeviceItem.qml:217
#, kde-format
msgid "Yes"
msgstr "Sí"

#: package/contents/ui/DeviceItem.qml:217
#, kde-format
msgid "No"
msgstr "Non"

#: package/contents/ui/DeviceItem.qml:223
#, kde-format
msgctxt "@label %1 is human-readable adapter name, %2 is HCI"
msgid "%1 (%2)"
msgstr "%1 (%2)"

#: package/contents/ui/DeviceItem.qml:231
#, kde-format
msgid "Remote Name"
msgstr ""

#: package/contents/ui/DeviceItem.qml:235
#, kde-format
msgid "Address"
msgstr ""

#: package/contents/ui/DeviceItem.qml:238
#, kde-format
msgid "Paired"
msgstr ""

#: package/contents/ui/DeviceItem.qml:241
#, kde-format
msgid "Trusted"
msgstr ""

#: package/contents/ui/DeviceItem.qml:244
#, kde-format
msgid "Adapter"
msgstr ""

#: package/contents/ui/DeviceItem.qml:252
#, kde-format
msgid "Disconnecting"
msgstr ""

#: package/contents/ui/DeviceItem.qml:252
#, kde-format
msgid "Connecting"
msgstr ""

#: package/contents/ui/DeviceItem.qml:258
#, kde-format
msgid "Connected"
msgstr ""

#: package/contents/ui/DeviceItem.qml:260
#, kde-format
msgid "Connection failed"
msgstr ""

#: package/contents/ui/DeviceItem.qml:267
#, kde-format
msgid "Audio device"
msgstr "Preséu d'audiu"

#: package/contents/ui/DeviceItem.qml:274
#, kde-format
msgid "Input device"
msgstr "Preséu d'entrada"

#: package/contents/ui/DeviceItem.qml:278
#, kde-format
msgid "Phone"
msgstr ""

#: package/contents/ui/DeviceItem.qml:285
#, kde-format
msgid "File transfer"
msgstr ""

#: package/contents/ui/DeviceItem.qml:288
#, kde-format
msgid "Send file"
msgstr ""

#: package/contents/ui/DeviceItem.qml:291
#, kde-format
msgid "Input"
msgstr ""

#: package/contents/ui/DeviceItem.qml:294
#, kde-format
msgid "Audio"
msgstr "Audiu"

#: package/contents/ui/DeviceItem.qml:297
#, kde-format
msgid "Network"
msgstr ""

#: package/contents/ui/DeviceItem.qml:301
#, kde-format
msgid "Other device"
msgstr "Otru preséu"

#: package/contents/ui/DeviceItem.qml:308 package/contents/ui/main.qml:80
#: package/contents/ui/main.qml:89
#, kde-format
msgid "%1% Battery"
msgstr ""

#: package/contents/ui/DeviceItem.qml:319
#, kde-format
msgctxt "Notification when the connection failed due to Failed:HostIsDown"
msgid "The device is unreachable"
msgstr ""

#: package/contents/ui/DeviceItem.qml:321
#, kde-format
msgctxt "Notification when the connection failed due to Failed"
msgid "Connection to the device failed"
msgstr ""

#: package/contents/ui/DeviceItem.qml:325
#, kde-format
msgctxt "Notification when the connection failed due to NotReady"
msgid "The device is not ready"
msgstr "El preséu nun ta preparáu"

#: package/contents/ui/DeviceItem.qml:361
#, kde-format
msgctxt ""
"@label %1 is human-readable device name, %2 is low-level device address"
msgid "%1 (%2)"
msgstr "%1 (%2)"

#: package/contents/ui/FullRepresentation.qml:64
#, kde-format
msgid "Enable"
msgstr ""

#: package/contents/ui/FullRepresentation.qml:160
#, kde-format
msgid "No Bluetooth adapters available"
msgstr ""

#: package/contents/ui/FullRepresentation.qml:162
#, kde-format
msgid "Bluetooth is disabled"
msgstr ""

#: package/contents/ui/FullRepresentation.qml:164
#, kde-format
msgid "No devices found"
msgstr "Nun s'atopó nengún preséu"

#: package/contents/ui/main.qml:58
#, kde-format
msgid "Bluetooth"
msgstr "Bluetooth"

#: package/contents/ui/main.qml:61
#, kde-format
msgid "Bluetooth is disabled; middle-click to enable"
msgstr ""

#: package/contents/ui/main.qml:65
#, kde-format
msgid "No adapters available"
msgstr ""

#: package/contents/ui/main.qml:67
#, kde-format
msgid "Bluetooth is offline"
msgstr ""

#: package/contents/ui/main.qml:70
#, kde-format
msgid "Middle-click to disable Bluetooth"
msgstr ""

#: package/contents/ui/main.qml:73
#, kde-format
msgid "No connected devices"
msgstr ""

#: package/contents/ui/main.qml:78
#, kde-format
msgid "%1 connected"
msgstr ""

#: package/contents/ui/main.qml:85
#, kde-format
msgctxt "Number of connected devices"
msgid "%1 connected device"
msgid_plural "%1 connected devices"
msgstr[0] ""
msgstr[1] ""

#: package/contents/ui/main.qml:147
#, kde-format
msgid "Add New Device…"
msgstr ""

#: package/contents/ui/main.qml:154
#, kde-format
msgid "Enable Bluetooth"
msgstr ""

#: package/contents/ui/main.qml:166
#, kde-format
msgid "Configure &Bluetooth…"
msgstr ""
