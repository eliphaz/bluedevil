# Copyright (C) 2024 This file is copyright:
# This file is distributed under the same license as the bluedevil package.
#
# SPDX-FileCopyrightText: 2024 Kishore G <kishore96@gmail.com>
msgid ""
msgstr ""
"Project-Id-Version: bluedevil\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2024-06-15 00:40+0000\n"
"PO-Revision-Date: 2024-06-16 17:17+0530\n"
"Last-Translator: Kishore G <kishore96@gmail.com>\n"
"Language-Team: Tamil <kde-i18n-doc@kde.org>\n"
"Language: ta\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 24.05.1\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "கோ. கிஷோர்"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "Kde-l10n-ta@kde.org"

#: bluetooth.cpp:102
#, kde-format
msgctxt "DeviceName Network (Service)"
msgid "%1 Network (%2)"
msgstr "%1 பிணையம் (%2)"

#: ui/Device.qml:108 ui/main.qml:194
#, kde-format
msgid "Disconnect"
msgstr "இணைப்பை துண்டி"

#: ui/Device.qml:108 ui/main.qml:194
#, kde-format
msgid "Connect"
msgstr "இணை "

#: ui/Device.qml:129
#, kde-format
msgid "Type:"
msgstr "வகை:"

#: ui/Device.qml:133
#, kde-format
msgid "%1%"
msgstr "%1%"

#: ui/Device.qml:135
#, kde-format
msgid "Battery:"
msgstr "மின்கலம்:"

#: ui/Device.qml:140 ui/General.qml:49
#, kde-format
msgid "Address:"
msgstr "முகவரி:"

#: ui/Device.qml:145
#, kde-format
msgid "Adapter:"
msgstr "செருகி"

#: ui/Device.qml:151 ui/General.qml:43
#, kde-format
msgid "Name:"
msgstr "பெயர்:"

#: ui/Device.qml:155
#, kde-format
msgid "Trusted"
msgstr "நம்பகமானது"

#: ui/Device.qml:161
#, kde-format
msgid "Blocked"
msgstr "தடுக்கப்பட்டது"

#: ui/Device.qml:168
#, kde-format
msgid "Send File"
msgstr "கோப்பை அனுப்பு"

#: ui/Device.qml:176
#, kde-format
msgid "Setup NAP Network…"
msgstr "NAP பிணையத்தை அமை…"

#: ui/Device.qml:184
#, kde-format
msgid "Setup DUN Network…"
msgstr "DUN பிணையத்தை அமை…"

#: ui/Device.qml:199
#, kde-format
msgctxt "@action:button Forget the Bluetooth device"
msgid "Forget"
msgstr "மறந்துவிடு"

#: ui/ForgetDeviceAction.qml:17
#, kde-format
msgctxt "@action:button %1 is the name of a Bluetooth device"
msgid "Forget \"%1\""
msgstr "\"%1\" தனை மறந்திடு"

#: ui/ForgetDeviceDialog.qml:34
#, kde-format
msgid "Forget this Device?"
msgstr "இந்த சாதனத்தை மறந்திடலாமா?"

#: ui/ForgetDeviceDialog.qml:35
#, kde-format
msgid "Are you sure you want to forget \"%1\"?"
msgstr "\"%1\" எனும் சாதனத்தை உறுதியாக மறந்திடலாமா?"

#: ui/ForgetDeviceDialog.qml:47
#, kde-format
msgctxt "@action:button"
msgid "Forget Device"
msgstr "சாதனத்தை மற"

#: ui/ForgetDeviceDialog.qml:54
#, kde-format
msgctxt "@action:button"
msgid "Cancel"
msgstr "ரத்து செய்"

#: ui/General.qml:24
#, kde-format
msgid "Settings"
msgstr "அமைப்புகள்"

#: ui/General.qml:35
#, kde-format
msgid "Device:"
msgstr "சாதனம்:"

#: ui/General.qml:53
#, kde-format
msgid "Enabled:"
msgstr "இயக்கப்பட்டதா:"

#: ui/General.qml:59
#, kde-format
msgid "Visible:"
msgstr "தெரிகிறதா:"

#: ui/General.qml:73
#, kde-format
msgid "On login:"
msgstr "நுழையும்போது:"

#: ui/General.qml:74
#, kde-format
msgid "Enable Bluetooth"
msgstr "ஊடலையை இயக்கு"

#: ui/General.qml:84
#, kde-format
msgid "Disable Bluetooth"
msgstr "ஊடலையை முடக்கு"

#: ui/General.qml:94
#, kde-format
msgid "Restore previous status"
msgstr "முந்தைய நிலையை மீட்டமை"

#: ui/General.qml:113
#, kde-format
msgid "When receiving files:"
msgstr "கோப்புகளை பெறும்போது:"

#: ui/General.qml:115
#, kde-format
msgid "Ask for confirmation"
msgstr "உறுதிப்படுத்து"

#: ui/General.qml:124
#, kde-format
msgid "Accept for trusted devices"
msgstr "நம்பகமான சாதனங்களிலிருந்து ஏற்றுக்கொள்"

#: ui/General.qml:134
#, kde-format
msgid "Always accept"
msgstr "எப்போதும் ஏற்றுக்கொள்"

#: ui/General.qml:144
#, kde-format
msgid "Save files in:"
msgstr "கோப்புகளை இங்கு சேமி:"

#: ui/General.qml:169 ui/General.qml:180
#, kde-format
msgid "Select folder"
msgstr "அடைவைத் தேர்ந்தெடு"

#: ui/main.qml:34
#, kde-format
msgctxt "@action: button as in, 'enable Bluetooth'"
msgid "Enabled"
msgstr "இயக்கு"

#: ui/main.qml:47
#, kde-format
msgid "Add New Device…"
msgstr "புதிய சாதனத்தை சேர்…"

#: ui/main.qml:53
#, kde-format
msgid "Configure…"
msgstr "அமை..."

#: ui/main.qml:119
#, kde-format
msgid "No Bluetooth adapters found"
msgstr "ஊடலை செருகியேதும் இல்லை"

#: ui/main.qml:120
#, kde-format
msgid "Connect an external Bluetooth adapter"
msgstr "வெளி ஊடலை செருகியை இணைக்கவும்"

#: ui/main.qml:129
#, kde-format
msgid "Bluetooth is disabled"
msgstr "ஊடலை முடக்கப்பட்டுள்ளது"

#: ui/main.qml:135
#, kde-format
msgid "Enable"
msgstr "இயக்கு"

#: ui/main.qml:145
#, kde-format
msgid "No devices paired"
msgstr "எச்சாதனமும் இணைக்கப்படவில்லை"

#: ui/main.qml:146
#, kde-kuit-format
msgctxt "@info"
msgid "Click <interface>Add New Device…</interface> to pair some"
msgstr "ஒன்றை சேர்க்க <interface>புதிய சாதனத்தை சேர்…</interface> என்பதை அழுத்தவும்"

#: ui/main.qml:170
#, kde-format
msgid "Connected"
msgstr "இணைந்துள்ளது"

#: ui/main.qml:170
#, kde-format
msgid "Available"
msgstr "கிடைக்கிறது"

#: ui/main.qml:229
#, kde-format
msgid "%1% Battery"
msgstr "%1% மின்கலம்"
